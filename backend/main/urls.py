from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='home'),
    path('copyrightholders', views.copyrightholders, name='copyrightholders'),
    path('popul', views.popul, name='popul'),
    path('cate', views.cate, name='cate'),
]
