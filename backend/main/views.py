from django.shortcuts import render
from journal.models import Journal
from category.models import Category

def index(request):
    journals = Journal.objects.order_by('-id')
    return render(request,'backend/index.html', {'title': 'Главная стрица сайта', 'journal': journals})

    
def copyrightholders(request):
    return render(request,'backend/copyrightholders.html')


def popul(request):
    return render(request,'backend/popul.html')
    

def cate(request):
    categorys = Category.objects.order_by('-id')
    return render(request,'backend/cate.html', {'title': 'Категории', 'category': categorys})