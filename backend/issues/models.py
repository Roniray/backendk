from django.db import models
from publication.models import Publication
from journal.models import Journal

class Issues(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    journals = models.ManyToManyField(verbose_name='Журналы', to=Journal, related_name='issuess')
    publications = models.ManyToManyField(verbose_name='Издательства', to=Publication, related_name='issuess')
    cover = models.ImageField(verbose_name='Обложка', upload_to='issuess/covers')

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Выпуск'
        verbose_name_plural = 'Выпуски'
