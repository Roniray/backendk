from django.contrib import admin
from issues.models import Issues

@admin.register(Issues)
class IssuesAdmin(admin.ModelAdmin):
    list_display = ("title","id","description", "cover",)
    list_filter = ("description",)
    search_fields = ("title",)

#admin.site.register(Issues)
