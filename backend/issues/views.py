from rest_framework.viewsets import ModelViewSet
from issues.serializers import IssuesSerializer
from issues.models import Issues


class IssuesViewSet(ModelViewSet):
    queryset = Issues.objects.all()
    serializer_class = IssuesSerializer


