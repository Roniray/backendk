from rest_framework.viewsets import ModelViewSet
from journal.serializers import JournalSerializer, FavoriteJournalSerializer
from journal.models import Journal, FavoriteJournal
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import NotFound


class JournalViewSet(ModelViewSet):
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer

    @action(methods=['POST'], detail=True, permission_classes=[IsAuthenticated], url_path='toggle-favorite')
    def toggle_favorite(self, request, pk=None):
        user = request.user
        try:
            journal = self.queryset.get(pk=pk)
        except Journal.DoesNotExist:
            raise NotFound('journal not found')
        try:
            fav_journal = FavoriteJournal.objects.get(user=user, journal=journal)
            fav_journal.delete()
            return Response({'message': 'removed'})
        except FavoriteJournal.DoesNotExist:
            FavoriteJournal.objects.create(user=user, journal=journal)
            return Response({'message': 'added'})

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='favorites')
    def get_favorites(self, request):
        user = request.user
        journals = FavoriteJournal.objects.filter(user=user)
        data = FavoriteJournalSerializer(instance=journals, many=True).data
        return Response(data)