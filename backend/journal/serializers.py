from rest_framework import serializers
from journal.models import FavoriteJournal, Journal
from publication.models import Publication
from authentication.serializers import User, UserSerializer
from rest_framework.permissions import IsAuthenticated


class PublicationSerializerForJournal(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ['nickname', 'date_of_foundation', ]


class JournalSerializer(serializers.ModelSerializer):
    publications_data = PublicationSerializerForJournal(source='publication', many=True)
    class Meta:
        model = Journal
        exclude = ['publication', ]


class FavoriteJournalSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user')
    journal_data = JournalSerializer(source='journal')

    class Meta:
        model = FavoriteJournal
        exclude = ['user', 'journal']
