from sre_constants import JUMP
from django.contrib import admin
from journal.models import Journal

@admin.register(Journal)
class JournalAdmin(admin.ModelAdmin):
    list_display = ("title","id","description", "cover", "photo_file", "released_at",)
    list_filter = ("id","title",)
    search_fields = ("title",)

#admin.site.register(Journal)