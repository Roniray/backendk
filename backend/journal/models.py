from django.db import models
from publication.models import Publication
from authentication.models import User


class Journal(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    publication = models.ManyToManyField(verbose_name='Издания',to=Publication, related_name='journals')
    description = models.TextField(verbose_name='Описание')
    photo_file = models.FileField(verbose_name='Файл', upload_to='journal/photo')
    cover = models.ImageField(verbose_name='Обложка', upload_to='journal/covers')
    released_at = models.DateField(verbose_name='Дата выпуска')

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Журнал'
        verbose_name_plural = 'Журналы'


class FavoriteJournal(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    journal = models.ForeignKey(Journal, verbose_name='User', on_delete=models.CASCADE)
