from rest_framework.viewsets import ModelViewSet
from publication.serializers import PublicationSerializer
from publication.models import Publication


class PublicationViewSet(ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer


