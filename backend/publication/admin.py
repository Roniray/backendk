from atexit import register
from django.contrib import admin
from publication.models import Publication

@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    list_display = ("nickname","id","publication_type", "Editor", "bio", "photo", "date_of_foundation",)
    list_filter = ("date_of_foundation","Editor",)
    search_fields = ("nickname","Editor",)

#admin.site.register(Publication)
