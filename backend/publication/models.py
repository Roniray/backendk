from django.db import models


class Publication(models.Model):
    nickname = models.CharField(verbose_name='Никнейм', max_length=255)
    publication_type = models.CharField(verbose_name='Тип публикации', max_length=255)
    Editor = models.CharField(verbose_name='Редактор', max_length=255)
    bio = models.TextField(verbose_name='Информация')
    date_of_foundation = models.DateField(verbose_name='Дата основания')
    photo = models.ImageField(verbose_name='Фото', upload_to='publications')

    def __str__(self):
        return self.nickname


    class Meta:
        verbose_name = 'Издательство'
        verbose_name_plural = 'Издательства'