from django.contrib import admin
from category.models import Category, Journal

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("title","id", "description", "cover",)
    list_filter = ("id","title",)
    search_fields = ("title",)

# admin.site.register(Category, CategoryAdmin)
