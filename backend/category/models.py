from django.db import models
from journal.models import Journal

class Category(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    journals = models.ManyToManyField(verbose_name=':Журналы', to=Journal, related_name='categorys')
    cover = models.ImageField(verbose_name='Обложка', upload_to='categorys/covers')

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
