from rest_framework.routers import DefaultRouter
from publication.views import PublicationViewSet
from journal.views import JournalViewSet
from genre.views import GenreViewSet
from category.views import CategoryViewSet
from issues.views import IssuesViewSet
from authentication.views import UserViewSet

router = DefaultRouter()

router.register('publication', PublicationViewSet)
router.register('journal', JournalViewSet)
router.register('genre', GenreViewSet)
router.register('category', CategoryViewSet)
router.register('issues', IssuesViewSet)
router.register('auth', UserViewSet)