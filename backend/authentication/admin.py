from django.contrib import admin
from authentication.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("email","id","bio", "first_name", "photo","is_superuser", "is_active",)
    list_filter = ("is_superuser","is_active","is_staff",)
    search_fields = ("email", "first_name",)

#admin.site.register(User)