from django.db import models
from journal.models import Journal

class Genre(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    journals = models.ManyToManyField(verbose_name=':Журналы', to=Journal, related_name='genres')
    cover = models.ImageField(verbose_name='Обложка', upload_to='genres/covers')

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'
        