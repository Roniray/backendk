from django.contrib import admin
from genre.models import Genre

@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ("title","id", "description", "cover")
    list_filter = ("id","title",)
    search_fields = ("title",)

# admin.site.register(Genre)